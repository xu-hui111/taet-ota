import { ref, onMounted } from 'vue'
// 数据  分页加载数据  总数据量
import { getRefundTicketListApi} from '@/api/refundApi'

export const ticketList = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page = 1,pageSize=10, search = null) => {
    //删除search里的空值
    for (let key in search) {
      if (!search[key]) {
        delete search[key]
      }
    }

    let ret = await getRefundTicketListApi(page,pageSize, search)
    console.log(ret)
    if (ret.data.ticketList.length > 0) {
      data.value = ret.data.ticketList
      total.value = ret.data.total
    }
    return ret.data.ticketList.length 
  }
  onMounted(() => {
    loadData()
  })

  return [data, loadData, total]
}
