import { ref, onMounted } from 'vue'
// 数据  分页加载数据  总数据量
import { getRefundHotelListApi} from '@/api/refundApi'

export const hotelList = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page = 1,pageSize=10, search = null) => {
    //删除search里的空值
    for (let key in search) {
      if (!search[key]) {
        delete search[key]
      }
    }

    let ret = await getRefundHotelListApi(page,pageSize, search)
    console.log(ret)
    if (ret.data.hotelList.length > 0) {
      data.value = ret.data.hotelList
      total.value = ret.data.total
    }
    return ret.data.hotelList.length 
  }
  onMounted(() => {
    loadData()
  })

  return [data, loadData, total]
}
