import { ref, onMounted } from 'vue'
// 数据  分页加载数据  总数据量
import { getHotelListApi } from '@/api/HotelInfoApi'

export const HotelList = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page = 1,pageSize=5, search = null) => {
    //删除search里的空值
    for (let key in search) {
      if (!search[key]) {
        delete search[key]
      }
    }

    let ret = await getHotelListApi(page,pageSize, search)
    console.log(ret.data)
    if (ret.data.records.length > 0) {
      data.value = ret.data.records
      total.value = ret.data.total
    }
    return ret.data.records.length 
  }
  onMounted(() => {
    loadData()
  })

  return [data, loadData, total]
}
