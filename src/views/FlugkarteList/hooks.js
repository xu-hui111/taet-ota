import { ref, onMounted } from 'vue'
// 数据  分页加载数据  总数据量
import { getFlugkarteListApi} from '@/api/FlugkarteListApi'

export const FlugkarteList = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page = 1,pageSize=5, search = null) => {
    let ret = await getFlugkarteListApi(page,pageSize, search)
    console.log(ret)
    if (ret.data.TicketList.length > 0) {
      data.value = ret.data.TicketList
      total.value = ret.data.total
    }
    return ret.data.TicketList.length
  }
  onMounted(() => {
    loadData()
  })

  return [data, loadData, total]
}