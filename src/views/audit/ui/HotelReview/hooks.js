import { ref, onMounted } from 'vue'
// 数据  分页加载数据  总数据量
import { getAuditHotelListApi} from '@/api/auditApi'

export const HotelList = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page = 1,pageSize=5, search = null) => {
    //删除search里的空值
    for (let key in search) {
      if (!search[key]) {
        delete search[key]
      }
    }
      console.log(page,pageSize, search);
    let ret = await getAuditHotelListApi(page,pageSize, search)
    console.log(ret)
    if (ret.data.Hotellist.length > 0) {
      data.value = ret.data.Hotellist
      total.value = ret.data.total
    }
    return ret.data.Hotellist.length 
  }
  onMounted(() => {
    loadData()
  })

  return [data, loadData, total]
}
