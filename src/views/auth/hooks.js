import { ref, onMounted } from 'vue'
import { getAuthsApi } from '@/api/authApi'

export const useAuths = () => {
  const data = ref([])

  const loadData = async (page=1,pageSize=5,search=null) => {
    const ret = await getAuthsApi(page,pageSize,search)
    console.log(ret.data.records)
    data.value = ret.data.records
    return ret.data.records.length
  }
  onMounted(() => {
    loadData(1,5,{status:'0'})
  })

  return [data, loadData]
}
