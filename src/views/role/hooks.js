import { ref, onMounted } from 'vue'
import { getRolesApi } from '@/api/roleApi'

export const useRoles = (pageNum = 1) => {
  const data = ref([])
  const total = ref(0)

  const loadData = async (page,pageSize,search) => {
    const ret = await getRolesApi(page=1,pageSize=10,search=null)
    if (ret.data.items.length > 0) {
      data.value = ret.data.items
      total.value = ret.data.total
    }
    return ret.data.items.length
  }
  onMounted(() => {
    loadData(pageNum,10,null)
  })

  return [data, loadData, total]
}
