import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// ant
import 'ant-design-vue/es/message/style/css'
import 'ant-design-vue/es/modal/style/css'

// 注册全局组件
import globalComponent from './components'

// 引入pinia
import { createPinia } from 'pinia'
import persistedState from 'pinia-plugin-persistedstate'


const pinia = createPinia()
pinia.use(persistedState)

const app = createApp(App)

// 注册全局属性
app.config.globalProperties.$host = import.meta.env.VITE_API_URL

// 注册全局组件
globalComponent(app)

app.use(pinia)
app.use(router)

app.mount('#app')
