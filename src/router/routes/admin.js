// export default [
//   {
//     path: '/dashoard',
//     component: () => import('@/views/dashoard/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '订单管理', url: '' },
//         { title: '新建机票', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/dashoard/addHotel',
//     component: () => import('@/views/addHotel/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '订单管理', url: '' },
//         { title: '新建酒店', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/dashoard/FlugkarteList',
//     component: () => import('@/views/FlugkarteList/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '订单管理', url: '' },
//         { title: '机票订单', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/dashoard/HotelList',
//     component: () => import('@/views/HotelList/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '订单管理', url: '' },
//         { title: '酒店订单', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/audit',
//     component: () => import('@/views/audit/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '订单审核', url: '' },
//         { title: '订单审核', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/finance/refund',
//     component: () => import('@/views/refund/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '财务管理', url: '' },
//         { title: '退款管理', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/finance/invoice',
//     component: () => import('@/views/invoice/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '财务管理', url: '' },
//         { title: '发票管理', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/sys/user',
//     component: () => import('@/views/user/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '后台设置', url: '' },
//         { title: '用户管理', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/sys/role',
//     component: () => import('@/views/role/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '后台设置', url: '' },
//         { title: '角色管理', url: '' }
//       ]
//     }
//   },
//   {
//     path: '/sys/auth',
//     component: () => import('@/views/auth/index.vue'),
//     meta: {
//       islogin: true,
//       crumb: [
//         { title: '首页', url: '/dashoard' },
//         { title: '后台设置', url: '' },
//         { title: '码管理', url: '' }
//       ]
//     }
//   }
// ]

export default [
  {
    path: "/dashoard",
    name: "订单管理",
    redirect: "/dashoard/addTicket",
    meta: {
      islogin: true,
      icon: "PieChartOutlined",
    },
    children: [
      {
        path: "/dashoard/addTicket",
        component: () => import("@/views/dashoard/index.vue"),
        name: "新建机票",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "新建机票", url: "" },
          ],
        },
      },
      {
        path: "/dashoard/addHotel",
        component: () => import("@/views/addHotel/index.vue"),
        name: "新建酒店",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "新建酒店", url: "" },
          ],
        },
      },
      {
        path: "/dashoard/FlugkarteList",
        component: () => import("@/views/FlugkarteList/index.vue"),
        name: "机票订单",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "机票订单", url: "" },
          ],
        },
      },
      {
        path: "/dashoard/FlugkarteList/FlugkarteDetails",
        component: () => import("@/views/FlugkarteList/datails/airDetails.vue"),
        name: "机票详情",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "机票订单", url: "" },
            { title: "机票详情", url: "" },
          ],
        },
      },
      {
        path: "/dashoard/HotelList",
        component: () => import("@/views/HotelList/index.vue"),
        name: "酒店订单",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "酒店订单", url: "" },
          ],
        },
      },
      {
        path: "/dashoard/HotelList/HotelDetails",
        component: () => import("@/views/HotelList/ui/createHotel.vue"),
        name: "酒店详情",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单管理", url: "" },
            { title: "新建酒店", url: "" },
            { title: "酒店详情", url: "" },
          ],
        },
      },
    ],
  },
  {
    path: "/audit",
    // component: () => import('@/views/audit/index.vue'),
    name: "订单审核",
    meta: {
      // islogin: true,
      icon: "userOutlined",
    },
    children: [
      {
        path: "/audit/orderAudit",
        redirect: "/audit/orderAudit/TicketReview",
        component: () => import("@/views/audit/index.vue"),
        name: "订单审核",
        meta: {
          // islogin: true,
          crumb: [
            { title: "订单审核", url: "" },
            { title: "订单审核", url: "" },
          ],
        },
        children: [
          {
            path: "TicketReview",
            component: () => import("@/views/audit/ui/TicketReview/index.vue"),
          },
          {
            path: "HotelReview",
            component: () => import("@/views//audit/ui/HotelReview/index.vue"),
          },
        ],
      },
    ],
  },
  {
    path: "/finance",
    name: "财务管理",
    meta: {
      // islogin: true,
      icon: "userOutlined",
    },
    children: [
      {
        path: "/finance/refund",
        redirect: "/finance/refund/ticketRefund",
        component: () => import("@/views/refund/index.vue"),
        name: "退款管理",
        meta: {
          // islogin: true,
          crumb: [
            { title: "财务管理", url: "" },
            { title: "退款管理", url: "" },
          ],
        },
        children: [
          {
            path: "ticketRefund",
            component: () => import("@/views/refund/ui/TicketRefund/index.vue"),
          },
          {
            path: "hotelRefund",
            component: () => import("@/views/refund/ui/HotelRefund/index.vue"),
          },
        ],
      },
      {
        path: "/finance/invoice",
        component: () => import("@/views/invoice/index.vue"),
        name: "发票管理",
        meta: {
          // islogin: true,
          crumb: [
            { title: "财务管理", url: "" },
            { title: "发票管理", url: "" },
          ],
        },
      },
      {
        path: "/finance/invoice/invoiceDetails",
        component: () => import("@/views/invoice/ui/invoiceDetails/index.vue"),
        meta: {
          // islogin: true,
          crumb: [
            { title: "财务管理", url: "" },
            { title: "发票管理", url: "" },
            { title: "发票详情", url: "" },
          ],
        },
      },
    ],
  },
  {
    path: "/sys",
    name: "系统设置",
    meta: {
      // islogin: true,
      icon: "userOutlined",
    },
    children: [
      {
        path: "/sys/user",
        component: () => import("@/views/user/index.vue"),
        name: "用户管理",
        meta: {
          // islogin: true,
          crumb: [
            { title: "后台设置", url: "" },
            { title: "用户管理", url: "" },
          ],
        },
      },
      {
        path: "/sys/role",
        component: () => import("@/views/role/index.vue"),
        name: "角色管理",
        meta: {
          // islogin: true,
          crumb: [
            { title: "后台设置", url: "" },
            { title: "角色管理", url: "" },
          ],
        },
      },
      {
        path: "/sys/auth",
        component: () => import("@/views/auth/index.vue"),
        name: "码管理",
        meta: {
          // islogin: true,
          crumb: [
            { title: "后台设置", url: "" },
            { title: "码管理", url: "" },
          ],
        },
      },
    ],
  },
];

//
