import { createRouter, createWebHistory } from 'vue-router'

import * as hooks from './hooks'
import routes from './common'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

Object.keys(hooks).forEach(key => {
  router.beforeEach(hooks[key]())
})

export default router
