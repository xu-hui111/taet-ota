import axios from 'axios'

const instance = axios.create({
  timeout: 10000,
  // 此处一定不要写http,有协议的话，则不会走代理
  baseURL: import.meta.env.VITE_API_URL
})

// 响应拦截器
instance.interceptors.response.use(
  res => res.data,
  err => Promise.reject(err)
)

// 请求拦截器
instance.interceptors.request.use(config => {
  return config
})

export const get = (url, config = {}) => instance.get(url, config)

export const del = (url, config = {}) => instance.delete(url, config)

export const post = (url, data = {}, config = {}) => instance.post(url, data, config)
