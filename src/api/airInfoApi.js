import {get,post} from "@/utils/http"

// 根据用户id获取乘机人信息
export const getAirInfoApi = uid => get(`/api/dashoard/${uid}`)

// 添加用户数据
export const addAirInfoApi = userData => post(`/api/dashoard/addTicket`, userData)

// 根据用户id获取入住人信息
export const getHotelInfoApi = uid => get(`/api/dashoard/addHotel/${uid}`)
// 添加入住人信息
export const addHotelInfoApi = userData => post(`/api/dashoard/addHotel`, userData)