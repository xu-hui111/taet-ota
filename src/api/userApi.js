import { post, get, del } from '@/utils/http'
import {jsonToSearch} from '@/utils/tools'

/**
 * 用户后台登录
 * @param {object} userData {username:'',password:''}
 * @returns Promise
 * loginApi({userame,pasword})
 */
export const loginApi = userData => post('/api/user/login', userData)

// 给定账号判断当前数据表中是否存在此用户
export const checkUsernameExistApi = username =>
  post('/api/checkUsernameExist', { username })

// 添加用户数据
export const addUserApi = userData => post(`/api/users`, userData)

// 修改用户数据
export const updateUserApi = (uid, userData) => post(`/api/users/${uid}`, userData)

// 用户列表数据
// searchData = {username: '', nickname: '', cdate: ''}
// username=&nickname=&cdate=
export const getUserListApi = (page = 1, searchData = null, pagesize = 10) => {
  if (searchData === null) {
    return get(`/api/userList?page=${page}&pagesize=${pagesize}`)
  }
  // typeof toString
  // let searchStr = Object.keys(searchData)
  //   .map(key => `${key}=${searchData[key]}`)
  //   .join('&')
  let searchStr = jsonToSearch(searchData)
  return get(`/api/userList?page=${page}&pagesize=${pagesize}&${searchStr}`)
}

// 根据用户id，返回对应用户信息
export const getUidToUserApi = uid => get(`/api/users/${uid}`)

// 删除指定用户
export const removeUidToUserApi = uid => del(`/api/users/${uid}`)

// 删除全部用户
export const removeAllUserApi = ids => post(`/api/removeAllUser`, { ids })
