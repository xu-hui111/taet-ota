import {get,post} from "@/utils/http"
import {jsonToSearch} from '@/utils/tools'
export const getFlugkarteListApi = (page = 1,pageSize = 5, searchData = null ) => {
    if (searchData === null) {
      return get(`/api/dashoard/FlugkarteList?page=${page}&pageSize=${pageSize}`)
    }
    let searchStr = jsonToSearch(searchData)
    return get(`/api/dashoard/FlugkarteList?page=${page}&pageSize=${pageSize}&${searchStr}`)
  }
