import {get,post} from "@/utils/http"
import {jsonToSearch} from '@/utils/tools'
export const getAuditHotelListApi = (page=1, pageSize=5,searchData=null  ) => {
    if (searchData === null) {
      return get(`/api/audit/orderAudit/HotelReview?page=${page}&pageSize=${pageSize}`)
    }

    let searchStr = jsonToSearch(searchData)
    return get(`/api/audit/orderAudit/HotelReview?page=${page}&pageSize=${pageSize}&${searchStr}`)
  }

  export const getsearchHotelApi=(data) =>post("/api/audit/orderAudit/HotelReview",data)


  // 机票订单
  export const getAuditTicketListApi = (data={page:1, pageSize:5,searchData:'1001'}) => {
    // if (data.searchData === '1001') {
    //   console.log(data.searchData)
      return post(`/api/audit/orderAudit/TicketReview`,{page:1, pageSize:5,searchData:'1001'})
    // }
    // /api/audit/orderAudit/TicketReview?page=1&pageSize=5&searchData='1001'
    // let searchStr = jsonToSearch(data.searchData)
    // return get(`/api/audit/orderAudit/TicketReview?page=${page}&pageSize=${pageSize}&${searchStr}`)
  }
  // /api/audit/orderAudit/TicketReview?page=1&pageSize=12&serchData
  export const getsearchTicketApi=(data) =>post("/api/audit/orderAudit/TicketReview",data)