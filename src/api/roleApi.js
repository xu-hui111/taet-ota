import { post, get, del } from '@/utils/http'
import { jsonToSearch } from '@/utils/tools'

export const getRolesApi = (page = 1,pageSize,search) => {
  if (search === null) {
    return get(`/api/roles?page=${page}&pageSize=${pageSize}`)
  }
  let searchStr = jsonToSearch(search)
  return get(`/api/roles?page=${page}&pageSize=${pageSize}&${searchStr}`)
}
export const addRoleApi = roleData => post(`/api/roles`, roleData)
export const updateRoleApi = (rid, roleData) => post(`/api/roles/${rid}`, roleData)
export const getRidToRoleApi = rid => get(`/api/roles/${rid}`)
export const removeRidToRoleApi = rid => del(`/api/roles/${rid}`)
