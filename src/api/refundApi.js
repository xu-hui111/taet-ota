import {get,post} from "@/utils/http"
import {jsonToSearch} from '@/utils/tools'
export const getRefundTicketListApi = (page = 1, searchData = null, pagesize = 10) => {
    if (searchData === null) {
      return get(`/api/finance/refund/ticketRefund?page=${page}&pagesize=${pagesize}`)
    }
    let searchStr = jsonToSearch(searchData)
    return get(`/api/finance/refund/ticketRefund?page=${page}&pagesize=${pagesize}&${searchStr}`)
  }

  export const getsearchRefundTicketApi=(data)=>post("/api/finance/refund/ticketRefund",data)
  export const getRefundHotelListApi = (page = 1, searchData = null, pagesize = 10) => {
    if (searchData === null) {
      return get(`/api/finance/refund/hotelRefund?page=${page}&pagesize=${pagesize}`)
    }
    let searchStr = jsonToSearch(searchData)
    return get(`/api/finance/refund/hotelRefund?page=${page}&pagesize=${pagesize}&${searchStr}`)
  }

  export const getsearchRefundHotelApi=(data)=>post("/api/finance/refund/hotelRefund",data)