import { post, get, del } from '@/utils/http'
import { jsonToSearch } from '@/utils/tools'

export const getAuthsApi = (page = 1, pageSize=5,search) => {
  if (search === null) {
    return get(`/api/auths?page=${page}&pageSize=${pageSize}`)
  }
  let searchStr = jsonToSearch(search)
  return get(`/api/auths?page=${page}&pageSize=${pageSize}&${searchStr}`)
}
export const addAuthApi = authData => post(`/api/auths`, authData)
export const updateAuthApi = (aid, authData) => post(`/api/auths/${aid}`, authData)
export const getAidToAuthApi = aid => get(`/api/auths/${aid}`)
export const removeAidToAuthApi = aid => del(`/api/auths/${aid}`)
