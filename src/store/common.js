import { defineStore } from 'pinia'

const useUserStore = defineStore('user', {
  persist: {
    key: 'user',
    storage: window.sessionStorage,
    paths: ['user']
  },
  state: () => ({
    user: {
      id: 0,
      nickname: '默认昵称',
      token: ''
    }
  }),
  actions: {
    setUser(user) {
      this.user = user
    }
  }
})

export default useUserStore