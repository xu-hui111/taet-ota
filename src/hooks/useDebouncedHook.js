import { customRef } from 'vue'
export default function useDebouncedRef(value, delay = 100) {
  let timer
  return customRef((track, trigger) => {
    return {
      get() {
        // 跟踪 记录下来
        track()
        return value
      },
      set(newValue) {
        timer && clearTimeout(timer)
        timer = setTimeout(() => {
          value = newValue
          // 数据有更新，通过刚才记录下来的目标更新
          trigger()
        }, delay)
      }
    }
  })
}
