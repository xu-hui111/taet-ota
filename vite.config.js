// nodejs环境
import { fileURLToPath, URL } from 'node:url'
import path from 'path'
import fs from 'fs'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
// 插件，用来嵌入到当前开发服务器中，让他可以提供mock机制
import { viteMockServe } from 'vite-plugin-mock'

import Components from 'unplugin-vue-components/vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'

const proxyDirPath = path.resolve('./proxy')
// 读取此目录下面所有的js文件
const dirs = fs.readdirSync(proxyDirPath)
const proxys = dirs
  .filter(file => file.includes('.js'))
  .map(file => require(`./proxy/${file}`))
  .reduce((prev, item) => Object.assign({}, prev, item), {})

const config = defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    viteMockServe({}),
    Components({
      resolvers: [AntDesignVueResolver()]
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    port: 8083,
    proxy: {
      // ...proxys
    }
  }
})

export default config
