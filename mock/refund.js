import Mock from 'mockjs'
export default [
    {
        url: '/api/finance/refund/ticketRefund',
        method: 'get',
        response: ({ body, query }) => {
            const ticketList = Array(50)
                .fill('')
                .map((_, index) => ({
                    num: "2016010500010",
                    route: "北京首都-上海虹桥",
                    passenger: ["张三 ","110104188001721234"],
                    principal: "李四 2015-10-10 10:10",
                    refundMoney: "300",
                    ...Mock.mock({
                        'refundState|1': ['未退款', '已退款']
                    }),
                }))

            return {
                code: 0,
                msg: 'ok',
                data: {
                    total: 50,
                    ticketList
                }
            }
        }
    },
    {
        url: '/api/finance/refund/ticketRefund',
        method: 'post',
        response: ({ body, query }) => {
            return {
                code: 0,
                msg: 'ok'
            }
        }
    },
    {
        url: '/api/finance/refund/hotelRefund',
        method: 'get',
        response: ({ body, query }) => {
            const hotelList = Array(50)
                .fill('')
                .map((_, index) => ({
                    num: "2016010500010",
                    hotelName: "富驿时尚酒店北京亦庄创意生活广场店",
                    bedType: "大床房",
                    ...Mock.mock({
                        'bedType|1': ['大床房', '标间']
                    }),
                    customer: "张三",
                    date: "2015-10-10",
                    applicant: "李四",
                    refundMoney: "300",
                    ...Mock.mock({
                        'refundState|1': ['未退款', '已退款']
                    }),
                }))

            return {
                code: 0,
                msg: 'ok',
                data: {
                    total: 50,
                    hotelList
                }
            }
        }
    },
    {
        url: '/api/finance/refund/hotelRefund',
        method: 'post',
        response: ({ body, query }) => {
            return {
                code: 0,
                msg: 'ok',
            }
        }
    }
]