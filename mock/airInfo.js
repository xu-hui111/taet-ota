import Mock from 'mockjs'
let TicketList=[
  {
    userId: 100001,
    username: '张三',
    phone: '13888888888',
    ID_type: '身份证号',
    ID_number: '123456789012345678',
    radioGroup: '1',
    weix:'zhangsan',
    tackOff_time:"2023-5-1",
    provider:"差旅天下",
    flightNum:"南航CZ3951",
    type:'空客A320',
    start:'北京',
    end:'上海',
    airport_start:'北京大兴',
    airport_end:"上海虹桥",
    departure_time:'13:50',
    landing_time:'15:50',
    shipping_space:'头等舱',
    level:'F',
    pnr:"KRZD9J",
    ParValue:'800',
    airrax:'200',
    bunkers:'50',
    insurance:'50',
    other:'30', 
  }
]
let createHotelList=[
  {
    HId: "200001",
    name: "张三",
    phone: "13888888888",
    weix: "zhangsan",
    provider:"差旅天下",
    HotelName:"如家",
    HotelTel:"88888888",
    checkIn_time:"2023-5-1",
    checkOut_time:"2023-5-2",
    address:"北京市昌平区如家酒店",
    house_type:"大床房",
    house_num:10,
    badType:"双人床",
    breakfast:"豆浆、油条、炒饭、鸡蛋",
    price:200,
    web:"wifi",
    roomFac:["1","3","4","5","7"],
    addCereal:"1",
    lastTime:"15"

  }
]
export default [
  {
    url: '/api/dashoard/HotelList',
    method: 'get',
    response: ({ body, query }) => {
      const Hotellist = Array(100)
        .fill('')
        .map((_, index) => ({
          key: index + 1,
          Hotelnum:202305010001+index,
          HotelName: "富驿时尚酒店北京亦庄创意生活广场店",
          ...Mock.mock({
            'HouseType|1':['大床房','标间']
          }),
          name: Mock.mock('@cname'),
          time:"2023-5-10",
          ...Mock.mock({
            'price|100-500':300
          }),
          principal: "李四",
          ...Mock.mock({
            'state|1': ['待发送','待支付','待确认','已完成']
          })
        }))

      return {
        code: 0,
        msg: 'ok',
        data: {
          total: 100,
          Hotellist
        }
      }
    }
  },
   // 显示机票列表信息
   {
    url: '/api/dashoard/FlugkarteList',
    method: 'get',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          total:10,
          // 返回添加后的当前此记录的id
          TicketList,
          // ...
        }
      }
    }
  },
  {
    url: '/api/dashoard/HotelList',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok'
      }
    }
  },
    {
      // 获取指定id的数据
      url: '/api/dashoard/:id',
      method: 'get',
      response: ({ body, query }) => ({
        code: 0,
        msg: 'ok',
        data: [
          {
            userId: 100001,
            username: '张三',
            phone: '13888888888',
            ID_type: '身份证号',
            ID_number: '123456789012345678',
            radioGroup: '1',
            weix:'zhangsan'
          },
          {
            userId: 100002,
            username: '李四',
            phone: '13888888888',
            ID_type: '身份证号',
            ID_number: '123456789012345678',
            radioGroup: '1',
            weix:'lisi'
          },
          {
            userId: 100003,
            username: '王五',
            phone: '13888888888',
            ID_type: '身份证号',
            ID_number: '123456789012345678',
            radioGroup: '1',
            weix:'wangwu'
          },
          {
            userId: 100004,
            username: '赵六',
            phone: '13888888888',
            ID_type: '身份证号',
            ID_number: '123456789012345678',
            radioGroup: '1',
            weix:'zhaoliu'
          },
        ]
      })
    },
    // 添加机票信息
    {
      url: '/api/dashoard/addTicket',
      method: 'post',
      response: ({ body, query }) => {
        let userid= TicketList.length+1
        TicketList.push({
          ...body,
          userid,
        })
        return {
          code: 0,
          msg: 'ok',
          data: {
            // 返回添加后的当前此记录的id
            TicketList,
            // ...
          }
        }
      }
    },
   
    
    {
      // 获取指定id的数据
      url: '/api/dashoard/addHotel/:id',
      method: 'get',
      response: ({ body, query }) => ({
        code: 0,
        msg: 'ok',
        data: [
          {
            HId: 200001,
            name: '张三',
            phone: '13888888888',
            weix:'zhangsan'
          },
          {
            HId: 200002,
            name: '李四',
            phone: '13888888888',
            weix:'lisi'
          },
          {
            HId: 200003,
            name: '王五',
            phone: '13888888888',
            weix:'wangwu'
          },
          {
            HId: 200004,
            name: '赵六',
            phone: '13888888888',
            weix:'zhaoliu'
          },
        ]
      })
    },
    // 添加酒店信息
    {
      url: '/api/dashoard/addHotel',
      method: 'post',
      response: ({ body, query }) => {
        let HId= createHotelList.length+1
        createHotelList.push({
          ...body,
          HId,
        })
        return {
          code: 0,
          msg: 'ok',
          data: {
            // 返回添加后的当前此记录的id
            createHotelList,
            // ...
          }
        }
      }
    },
  ]