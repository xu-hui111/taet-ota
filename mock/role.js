import Mock from 'mockjs'

let roles = [
  /**
   * id	ID	Number
rolename	角色名	String
note	备注	String
createUser	创建人	String
createTIme	创建时间	data
currentUserList	当前是该角色用户列表	array	item：{ID、用户名、姓名}
notCurrentUserList	不是当前该角色用户列表	array	同上
permissions	权限列表	Object	有权限为true。无权限为false
   */
  {
    id: 1,
    rolename: '超级管理员',
    note: '超级管理员',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    currentUserList: [
      {
        id: 1,
        username: 'admin',
        nickname: '张三'
      }
    ],
    notCurrentUserList: [
      {
        id: 2,
        username: 'guest',
        nickname: '李四'
      }
    ],
    permissions: {
      // 菜单权限
      dashoard: true,
      addTicket: true,
      addHotel: true,
      FlugkarteList: true,
      HotelList: true,
      audit: true,
      finance: true,
      refund: true,
      invoice: true,
      sys: true,
      user: true,
      auth: true,
      role: true
    }
  },
  {
    id: 2,
    rolename: '运营',
    note: '运营',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    currentUserList: [
      {
        id: 2,
        username: 'guest',
        nickname: '李四'
      }
    ],
    notCurrentUserList: [
      {
        id: 1,
        username: 'admin',
        nickname: '张三'
      }
    ],
    permissions: {
      // 菜单权限
      dashoard: true,
      addTicket: true,
      addHotel: true,
      FlugkarteList: true,
      HotelList: true,
      audit: true,
      finance: true,
      refund: true,
      invoice: true,
      sys: false,
      user: false,
      auth: false,
      role: false
    }
  }
]


export default [
  {
    url: '/api/roles',
    method: 'get',
    response: ({ body, query }) => {
      const { page, pageSize,rolename } = query
      let _page = Number(page)
      let _pageSize = Number(pageSize)
      let ret = roles
      const start = (_page - 1) * _pageSize
      const end = _page * _pageSize
      if(rolename){
        ret = ret.filter(item=>item.rolename.indexOf(rolename)>-1)
      }
      ret = ret.slice(start, end)
      return {
        code: 0,
        msg: 'ok',
        data: {
          total: 20,
          page: page,
          pageSize: pageSize,
          items: ret
        }
      }
    }
  },
  {
    url: '/api/roles/:id',
    method: 'get',
    response: ({ body, query }) => {
      const { id } = query
      const ret = roles.find(item => item.id == id)
      return {
        code: 0,
        msg: 'ok',
        data: ret
      }
    }
  },
  {
    url: '/api/roles/:id',
    method: 'post',
    response: ({ body, query }) => {
      roles = roles.map(item => {
        if (item.id == body.id) {
          return body
        }
        return item
      }
      )
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: query.id
        }
      }
    }
  },
  {
    url: '/api/roles/:id',
    method: 'delete',
    response: ({ body, query }) => {
      roles = roles.filter(item => item.id != query.id)
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: query.id
        }
      }
    }
  },
  {
    url: '/api/roles',
    method: 'post',
    response: ({body}) => {
      body.id = roles.length + 1
      roles.push(body)
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: body.id
        }
      }
    }
  }
]
