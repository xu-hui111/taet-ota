import Mock from 'mockjs'
export default [
  {
    url: '/api/audit/orderAudit/HotelReview',
    method: 'get',
    response: ({ body, query }) => {
      const Hotellist = Array(50)
        .fill('')
        .map((_, index) => ({
            num:"2016010500010",
            hotelName: "富驿时尚酒店北京亦庄创意生活广场店",
            ...Mock.mock({
                'bedType|1':['大床房','标间']
              }),
            customer: Mock.mock('@cname'),
            date: "2023-5-10",
            orderState: "待确认",
            ...Mock.mock({
                'reason|1': ['取消订单','申请退款']
              }),
            orderAmount: "300",
            applicant: "李四",
        }))

      return {
        code: 0,
        msg: 'ok',
        data: {
          total: 50,
          Hotellist
        }
      }
    }
  },
  {
    url: '/api/audit/orderAudit/HotelReview',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok'
      }
    }
  },
  {
    url: '/api/audit/orderAudit/TicketReview',
    method: 'get',
    response: ({ body, query }) => {
      const TicketList = Array(50)
        .fill('')
        .map((_, index) => ({
            num: "2016010500010",
            route: "北京首都-上海虹桥",
            passenger:"张三",
            orderState: "待出票",
            ...Mock.mock({
                'reason|1': ['取消订单','申请退款']
              }),
            orderAmount: "300",
            applicant: "李四",
        }))

      return {
        code: 0,
        msg: 'ok',
        data: {
          total: 50,
          TicketList
        }
      }
    }
  },
  {
    url: '/api/audit/orderAudit/TicketReview',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok'
      }
    }
  },
]