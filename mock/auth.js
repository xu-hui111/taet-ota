import Mock from 'mockjs'

const authList = [
  {
    id: 1,
    code: '12939213909',
    type: '贵宾室',
    merchants: '商户1',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    status: '1',
  },
  {
    id: 2,
    code: '12939213910',
    type: '贵宾室',
    merchants: '商户2',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    status: '0',
  },
]

export default [
  {
    url: '/api/auths',
    method: 'get',
    response: ({ body, query }) => {
      const { page = 1, pageSize = 10, status, type, createUser, code, merchants } = query
      const start = (page - 1) * pageSize
      const end = page * pageSize

      let auths = authList

      if (status) {
        auths = auths.filter(item => item.status === status)
      }

      if (type) {
        auths = auths.filter(item => item.type === type)
      }

      if (createUser) {
        auths = auths.filter(item => item.createUser === createUser)
      }

      if (code) {
        auths = auths.filter(item => item.code === code)
      }

      if (merchants) {
        auths = auths.filter(item => item.merchants === merchants)
      }

      auths = auths.slice(start, end)

      return {
        code: 0,
        msg: 'ok',
        data: {
          total: authList.length,
          page,
          pageSize,
          items: auths
        }
      }

    }
  },
  {
    url: '/api/auths/:id',
    method: 'get',
    response: ({ body, query }) => {
      const auth = authList.find((item) => item.id === +query.id)
      if (!auth) {
        return {
          code: 1,
          msg: '用户不存在',
          data: {}
        }
      }
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...auth,
        }
      }
    }
  },
  {
    url: '/api/auths',
    method: 'post',
    response: ({ body, query }) => {
      authList.push({
        ...body,
        id: Mock.mock('@increment(3)'),
        createTime: new Date().toLocaleString()
      })
      return {
        code: 0,
        msg: 'ok',
        data: {}
      }
    }
  },
  {
    url: '/api/auths/:id',
    method: 'post',
    response: ({ body, query }) => {
      const auth = authList.find((item) => item.id === +query.id)
      if (!auth) {
        return {
          code: 1,
          msg: '用户不存在',
          data: {}
        }
      }
      Object.assign(auth, body)
      return {
        code: 0,
        msg: 'ok',
        data: {}
      }
    }
  },
  {
    url: '/api/auths/:id',
    method: 'delete',
    response: ({ body, query }) => {
      const index = authList.findIndex((item) => item.id === +query.id)
      if (index === -1) {
        return {
          code: 1,
          msg: '用户不存在',
          data: {}
        }
      }
      authList.splice(index, 1)
      return {
        code: 0,
        msg: 'ok',
        data: {}
      }
    }
  }
]

    //
