import Mock from 'mockjs'

let userList = [
  {
    id: 1,
    username: 'admin',
    password: 'admin',
    nickname: '张三',
    rolename: '超级管理员',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    status: '1', // 1正常，0禁用
    permissions: {
      // 菜单权限
      dashoard: true,
      addTicket: true,
      addHotel: true,
      FlugkarteList: true,
      HotelList: true,
      audit: true,
      finance: true,
      refund: true,
      invoice: true,
      sys: true,
      user: true,
      auth: true,
      role: true
    }
  },
  {
    id: 2,
    username: 'guest',
    password: 'guest',
    nickname: '李四',
    rolename: '运营',
    createUser: 'admin',
    createTime: '2020-12-12 12:12:12',
    status: '1', // 1正常，0禁用
    permissions: {
      // 菜单权限
      dashoard: true,
      addTicket: true,
      addHotel: true,
      FlugkarteList: true,
      HotelList: true,
      audit: true,
      finance: true,
      refund: true,
      invoice: true,
      sys: false,
      user: false,
      auth: false,
      role: false
    }
  },
]


export default [
  {
    url: '/api/user/login',
    method: 'post',
    response: ({ body, query }) => {
      const { username, password } = body
      const user = userList.find(
        (item) => item.username === username && item.password === password
      )
      if (!user) {
        return {
          code: 1,
          msg: '用户名或密码错误',
          data: {}
        }
      }
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: user.id,
          roleName: user.rolename,
          token: 'fewlfjekwlfjewklfjelfjek;kelfelfewl',
          loginTime: new Date().getTime(),
          username: user.username,
          nickname: user.nickname,
          permissions: user.permissions
        }
      }
    }
  },
  {
    url: '/api/checkUsernameExist',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok',
        // 0用户名不存，1存在
        // select count(id) as num from xx where username=`用户名`
        data: 0
      }
    }
  },
  {
    url: '/api/userupload',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          imgurl:
            'https://up.enterdesk.com/edpic_source/dd/3a/18/dd3a18ad1194e0190e96ce9eb2e14164.jpg',
          path: 'public/headimg.png'
        }
      }
    }
  },
  {
    url: '/api/users',
    method: 'post',
    response: ({ body, query }) => {
      const id = userList.length + 1
      userList.push({
        ...body,
        createTime: new Date().toLocaleString(),
        id,
      })
      return {
        code: 0,
        msg: 'ok',
        data: {
          // 返回添加后的当前此记录的id
          id,
          // ...
        }
      }
    }
  },
  {
    url: '/api/userList',
    method: 'get',
    response: ({ body, query }) => {
      // const users = Array(10)
      //   .fill('')
      //   .map((_, index) => ({
      //     id: index + 1,
      //     username: Mock.mock('@name'),
      //     nickname: Mock.mock('@cname'),
      //     ...Mock.mock({
      //       'age|1-200': 22
      //     }),
      //     ...Mock.mock({
      //       'sex|1-2': '1'
      //     }),
      //     cdate: Mock.mock('@date()')
      //   }))
      let users = userList
      const { page = 1, pageSize = 10 } = query
      const start = (page - 1) * pageSize
      const end = page * pageSize

      const username = query.username || ''
      const nickname = query.nickname || ''

      if(username){
        users = users.filter(item => item.username === username)
      }

      if(nickname){
        users = users.filter(item => item.nickname === nickname)
      }


      users = users.slice(start,end)


      return {
        code: 0,
        msg: 'ok',
        data: {
          total: userList.length,
          page,
          pageSize,
          items: users
        }
      }
    }
  },
  {
    url: '/api/users/:id',
    method: 'get',
    response: ({ body, query }) => {
      const user = userList.find((item) => item.id === +query.id)
      if (!user) {
        return {
          code: 1,
          msg: '用户不存在',
          data: {}
        }
      }
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...user,
        }
      }
    }
  },
  {
    url: '/api/users/:id',
    // patch/put
    method: 'post',
    response: ({ body, query }) => {
      userList = userList.map((item) => {
        if (item.id === +query.id) {
          item = {
            ...item,
            ...body
          }
        }
        return item
      })


      return {
        code: 0,
        msg: 'ok',
        data: {
          id: query.id
        }
      }
    }
  },
  {
    url: '/api/users/:id',
    method: 'delete',
    response: ({ body, query }) => {
      userList = userList.filter((item) => item.id !== +query.id)
      return {
        code: 0,
        msg: 'ok',
        data: {
          id: query.id
        }
      }
    }
  },
  {
    url: '/api/removeAllUser',
    method: 'post',
    response: ({ body, query }) => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  }
]
