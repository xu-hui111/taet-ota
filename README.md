# 差旅服务OTA后台管理系统
## 介绍
> 该系统主要实现机票酒店的数据增加，机票退改签，酒店取消订单，财务管理以及用户权限管理操作
## 技术栈
>vue3 + pinia + vue-router + vite + ES6+ + axios + mockjs +  Ant组件库 + echarts + 其他
## 项目预览
> http://xxx.com/

## 依赖包版本
nodejs v16.14.2
├── @vitejs/plugin-vue-jsx@2.1.1
├── @vitejs/plugin-vue@3.2.0
├── ant-design-vue@3.2.19
├── axios@1.3.6
├── mockjs@1.1.0
├── pinia-plugin-persistedstate@3.1.0
├── pinia@2.0.34
├── sass@1.62.0
├── unplugin-vue-components@0.24.1
├── vite-plugin-mock@2.9.8
├── vite@3.2.6
├── vue-router@4.1.6
└── vue@3.2.47


## 路由搭建
实现页面的基础框架的搭建，包括登录页面，首页及缺省页面的路由信息，实现侧边菜单导航路由与右侧内容联动

## 登录页面

## 新建机票

##  新建酒店

##  机票  酒店订单

##  订单审核

##  退款管理

##  发票管理

##  码管理

##  用户管理

## 角色管理
