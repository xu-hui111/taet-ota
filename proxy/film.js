// 代理只能在开发环境中使用，在生产环境中不能使用vite中的代理
// 生产环境中
//   + 接口允许跨域
//   + 编写代理服务器，进行代理 [nodejs]
const film = {
  '/api': {
    target: 'https://api.iynn.cn/film',
    changeOrigin: true,
    // rewrite: path => path.replace(/^\/abc/, '')
  }
}

// 如果是自动导入,用commonjs模块化
module.exports = film
